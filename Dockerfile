# Uses multistage build in docker 17.05

# Build Container
FROM golang:alpine
RUN apk -U add make git openssh
RUN go get github.com/golang/dep/cmd/dep
WORKDIR /go/src/gitlab.com/t4spartners/worp
ADD . .
RUN make deps; make install

# Final Build Image
FROM alpine:latest

RUN apk -U add ca-certificates
COPY --from=0 /go/bin/worp /bin
ENTRYPOINT ["/bin/worp"]
