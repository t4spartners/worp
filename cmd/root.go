// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	csm "gitlab.com/t4spartners/csm-go-lib"
	"gitlab.com/t4spartners/worp/server"

	"github.com/fsnotify/fsnotify"
	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	version string
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "worp",
	Short: "Cherwell Worp Server",
	Run: func(cmd *cobra.Command, args []string) {
		if viper.GetBool("version") {
			fmt.Printf("CSM Worp Server %s\n", version)
			return
		}

		var config server.Config
		if err := viper.Unmarshal(&config); err != nil {
			log.Fatal(err)
		} else if viper.GetBool("verbose") {
			log.Printf("using config: %+v", config)
		}

		app := server.Worp{Router: mux.NewRouter(), Config: &config, Sessions: make(map[string]*csm.Session)}
		app.InitializeRoutes()

		if config.SSL {
			log.Printf("Using Cherwell instance over SSL: %s\n", config.Server)
			app.CSM = csm.NewSessionSSL(config.Server)
		} else {
			log.Printf("Using Cherwell instance: %s\n", config.Server)
			app.CSM = csm.NewSession(config.Server)
		}

		if viper.GetBool("vv") {
			app.CSM.WithDebug()
		}

		viper.WatchConfig()
		viper.OnConfigChange(func(e fsnotify.Event) {
			log.Println("Config file changed:", e.Name)
			if err := viper.ReadInConfig(); err == nil {
				log.Println("Using config file:", viper.ConfigFileUsed())
				var config server.Config
				if err := viper.Unmarshal(&config); err != nil {
					log.Println("could not read config file:", err)
					return
				}
				app.Config = &config
			}
		})

		log.Printf("listening (%s)", viper.GetString("port"))
		log.Fatal(http.ListenAndServe(":"+viper.GetString("port"), app.Router))
	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.worp.yml)")

	RootCmd.PersistentFlags().BoolP("verbose", "v", false, "verbose output")
	viper.BindPFlag("verbose", RootCmd.PersistentFlags().Lookup("verbose"))

	RootCmd.PersistentFlags().Bool("vv", false, "extra verbose")
	viper.BindPFlag("vv", RootCmd.PersistentFlags().Lookup("vv"))

	RootCmd.Flags().BoolP("version", "V", false, "print version")
	viper.BindPFlag("version", RootCmd.Flags().Lookup("version"))

	RootCmd.Flags().StringP("port", "p", "8080", "port to listen on")
	viper.BindPFlag("port", RootCmd.Flags().Lookup("port"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetDefault("server", "")
	viper.SetDefault("ssl", false)

	viper.SetEnvPrefix("WORP")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName(".worp")
	viper.AddConfigPath("$HOME")
	if err := viper.ReadInConfig(); err == nil {
		if viper.GetBool("verbose") {
			log.Println("Using config file:", viper.ConfigFileUsed())
		}
	} else {
		if viper.GetBool("verbose") {
			log.Println("err during read config:", err)
		}
	}
}
