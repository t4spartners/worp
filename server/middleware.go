// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package server

import (
	"log"
	"net/http"
	"strings"

	"gitlab.com/t4spartners/csm-go-lib/client/service"

	"github.com/go-openapi/runtime"
	httptransport "github.com/go-openapi/runtime/client"
)

func (a *Worp) Authorize(h func(http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s Request %s", r.Host, r.URL)
		defer recoverAuthorized(w, r)

		if t := r.Header.Get("authorization"); t != "" {
			if _, ok := a.Sessions[t]; !ok {
				s := a.CSM.Clone().WithAuth(authorize(r))
				a.Sessions[t] = s
			}
		} else {
			// Request not authorized
			panic(&service.ServiceTokenBadRequest{})
		}
		h(w, r)
	})
}

func authorize(r *http.Request) func() runtime.ClientAuthInfoWriter {
	return func() runtime.ClientAuthInfoWriter {
		if t := r.Header.Get("authorization"); t == "" {
			panic(&service.ServiceTokenBadRequest{})
		} else {
			return httptransport.BearerToken(strings.Trim(strings.Trim(t, "Bearer "), "bearer "))
		}
	}
}

func recoverAuthorized(w http.ResponseWriter, r *http.Request) {
	if rec := recover(); rec != nil {
		switch rec.(type) {
		case *service.ServiceTokenBadRequest:
			log.Printf("Authentication Failure from %s", r.Host)
			respondWithError(w, http.StatusUnauthorized, "Request Not Authorized")
		default:
			respondWithError(w, http.StatusInternalServerError, "server panic")
			panic(rec)
		}
	}
}
